const path = require('path');

module.exports = {
  webpackFinal: async (config, { configType }) => {
    config.resolve.modules = [
      path.resolve(__dirname, ".."),
      "node_modules",
    ]

    config.resolve.alias = {
      ...config.resolve.alias,
      "~/components": path.resolve(__dirname, "../src/components"),
      "~/styles": path.resolve(__dirname, "../src/styles"),
      "~/i18n": path.resolve(__dirname, "../src/i18n"),
      "~/utils": path.resolve(__dirname, "../src/utils"),
      "~/hooks": path.resolve(__dirname, "../src/hooks")
    };

    return config;
  },
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ]
}