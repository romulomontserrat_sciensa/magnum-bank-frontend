import React from 'react';
import Link from 'next/link';
import { Screen, Text } from '~/components';

const About = () => (
  <Screen title="About">
    <Text variant="h2">
      About
    </Text>
    <Link href="/">Home</Link>
  </Screen>
);

export default About;
