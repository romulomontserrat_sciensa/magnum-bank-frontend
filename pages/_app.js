/* eslint-disable react/prop-types */

import React from 'react';
import { ThemeProvider } from 'styled-components';
import { appWithTranslation } from '~/i18n';
import { useNProgress } from '~/hooks';
import { GlobalStyle, theme } from '~/styles';

const App = ({ Component, pageProps }) => {
  useNProgress();

  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
};

export default appWithTranslation(App);
