import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { Screen, Text } from '~/components';
import { withTranslation } from '~/i18n';

const Home = ({ t }) => (
  <Screen title={t('title')}>
    <Text variant="h2">
      {t('title')}
    </Text>
    <Link href="/about">Sobre</Link>
  </Screen>
);

Home.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation('common')(Home);
