const { nextI18NextRewrites } = require('next-i18next/rewrites');

const localeSubpaths = {};

const path = require('path');
const withPlugins = require('next-compose-plugins');
const images = require('next-images');
const css = require('@zeit/next-css');

const cssConfig = {
  cssModules: true,
};

const imagesConfig = {
  exclude: path.resolve(__dirname, 'src/assets/icons'),
};

const nextConfig = {
  webpack: (config) => config,
  rewrites: async () => nextI18NextRewrites(localeSubpaths),
  publicRuntimeConfig: {
    localeSubpaths,
  },

};

module.exports = withPlugins([
  [css, cssConfig],
  [images, imagesConfig],
], nextConfig);
