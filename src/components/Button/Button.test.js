import React from 'react';
import { screen } from '@testing-library/react';
import { render } from '~/utils/tests';
import 'jest-styled-components';
import '@testing-library/jest-dom/extend-expect';
import Button from './Button';

describe('<Button />', () => {
  test('it should mount', () => {
    render(<Button>Button Test</Button>);

    const button = screen.getByTestId('Button');

    expect(button).toBeInTheDocument();
  });

  test('snapshot', () => {
    const { asFragment } = render(
      <Button>Button Test</Button>,
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
