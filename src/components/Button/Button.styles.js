import styled from 'styled-components';

export const StyledButton = styled.button`
  border: 0;
  cursor: pointer;
  outline: none;
  width: 100%;
  padding: 15px 24px;
  border-radius: 4px;
  font-family: ${(props) => props.theme.fontFamily};
  font-size: ${(props) => props.theme.fontSizes.medium};
  font-weight: ${(props) => props.theme.fontBold};
`;
