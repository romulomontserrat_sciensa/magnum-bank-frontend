import React, { isValidElement } from 'react';

import { Wrapper } from './Text.styles';

const TAGS = ['h1', 'h2', 'h3', 'h4'];

const Text = ({
  as,
  children,
  isError,
  isLink,
  isRichText,
  variant,
  ...props
}) => {
  const shouldChangeTag = !!variant && TAGS.includes(variant);
  const tag = as ?? (shouldChangeTag && variant);

  const renderText = (text, index) => (
    <Wrapper
      key={`text-${index}`}
      data-testid="text"
      variant={variant}
      isError={isError}
      isLink={isLink}
      {...(tag && { as: tag })}
      {...props}
    >
      {text}
    </Wrapper>
  );

  if (isRichText && isValidElement(children)) {
    return children?.props?.children.map((item) => renderText(item?.props?.children ?? item));
  }

  return renderText(children);
};

export default Text;
