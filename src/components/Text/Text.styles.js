import styled, { css } from 'styled-components';

const H1 = css`
  font-size: ${(props) => props.theme.fontSizes.xxbig};
  line-height: 32px;
  font-weight: ${(props) => props.theme.fontBold};
`;

const H2 = css`
  font-size: ${(props) => props.theme.fontSizes.xbig};
  line-height: 28px;
  font-weight: ${(props) => props.theme.fontBold};
`;

const H3 = css`
  font-size: ${(props) => props.theme.fontSizes.big};
  line-height: 28px;
  font-weight: ${(props) => props.theme.fontBold};
`;

const H4 = css`
  font-size: ${(props) => props.theme.fontSizes.medium};
  line-height: 24px;
  font-weight: ${(props) => props.theme.fontBold};
`;

const Small = css`
  font-size: ${(props) => props.theme.fontSizes.small};
  line-height: 18px;
`;

const XSmall = css`
  font-size: ${(props) => props.theme.fontSizes.xsmall};
  line-height: 18px;
`;

const variants = {
  h1: H1,
  h2: H2,
  h3: H3,
  h4: H4,
  small: Small,
  xsmall: XSmall,
};

export const Wrapper = styled.p`
  white-space: pre-wrap;
  font-size: ${(props) => props.theme.fontSizes.small};
  line-height: 2px;

  ${({ variant }) => variants[variant]};

  ${({ isError }) => isError && `
      color: ${(props) => props.theme.colors.danger};
    `}

  ${({ isLink }) => isLink && `
    font-weight: ${(props) => props.theme.fontRegular};
    color: ${(props) => props.theme.colors.secondary};
    cursor: pointer;
  `}
`;
