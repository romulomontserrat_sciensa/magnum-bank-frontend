import React from 'react';
import { screen } from '@testing-library/react';
import { render } from '~/utils/tests';
import 'jest-styled-components';
import '@testing-library/jest-dom/extend-expect';
import Text from './Text';

describe('<Text />', () => {
  test('it should mount', () => {
    render(<Text>Text Test</Text>);

    const text = screen.getByTestId('text');

    expect(text).toBeInTheDocument();
  });

  test('snapshot', () => {
    const { asFragment } = render(
      <Text>Text Test</Text>,
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
