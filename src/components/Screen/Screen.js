/* eslint-disable import/no-cycle */

import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import favicon from '../../assets/images/favicon.ico';

import { StyledScreen } from './Screen.styles';

const Screen = ({ children, title }) => (
  <StyledScreen data-testid="Screen">
    <Head>
      <meta name="viewport" content="width=device-width" />
      <meta name="description" content="Nextjs boilerplate" />
      <title>{title}</title>
      <link rel="icon" href={favicon} />
    </Head>
    {children}
  </StyledScreen>
);

Screen.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
};

Screen.defaultProps = {
  title: 'Magnum Bank',
};

export default Screen;
