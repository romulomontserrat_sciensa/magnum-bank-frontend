import styled from 'styled-components';

export const StyledScreen = styled.main`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  min-height: 100vh;
`;
