/* eslint-disable no-undef */
import React from 'react';
import preloadAll from 'jest-next-dynamic';
import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils';
import { ThemeProvider } from 'styled-components';
import { render as rtlRender } from '@testing-library/react';
import { RouterContext } from 'next/dist/next-server/lib/router-context';
import { theme } from '~/styles';

beforeAll(async () => {
  await preloadAll();

  mockAllIsIntersecting(true);
});

afterAll(() => {
  jest.clearAllMocks();
});

const noop = () => {};

Object.defineProperty(window, 'scrollTo', { value: noop, writable: true });

window.matchMedia = window.matchMedia
  || function matchMedia() {
    return {
      matches: false,
      addListener() {},
      removeListener() {},
    };
  };

const mockRouter = {
  basePath: '',
  pathname: '/',
  route: '/',
  asPath: '/',
  query: {},
  push: jest.fn(),
  replace: jest.fn(),
  reload: jest.fn(),
  back: jest.fn(),
  prefetch: jest.fn(),
  beforePopState: jest.fn(),
  events: {
    on: jest.fn(),
    off: jest.fn(),
    emit: jest.fn(),
  },
  isFallback: false,
};

function render(
  ui,
  { router, ...renderOptions } = {},
) {
  // eslint-disable-next-line react/prop-types
  function Wrapper({ children }) {
    return (
      <RouterContext.Provider value={{ ...mockRouter, ...router }}>
        <ThemeProvider theme={theme}>
          {children}
        </ThemeProvider>
      </RouterContext.Provider>
    );
  }

  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}

export * from '@testing-library/react';

export { render };
