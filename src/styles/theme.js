import colors from './colors';

const theme = {
  colors,
  fontFamily: '-apple-system, blinkmacsystemfont, segoe ui, roboto, oxygen, ubuntu, cantarell, fira sans, droid sans, helvetica neue, sans-serif',
  fontBold: 600,
  fontRegular: 400,
  fontThin: 300,
  fontSizes: {
    xxbig: '36px',
    xbig: '24px',
    big: '18px',
    medium: '16px',
    small: '14px',
    xsmall: '12px',
  },
};

export default theme;
