const breakpoints = {
  lg: '@media all and (min-width: 1024px)',
  md: '@media all and (min-width: 768px) and (max-width: 1023px)',
  sm: '@media all and (max-width: 767px)',
  ltsm: '@media all and (min-width: 768px)',
  ltlgsm: '@media all and (min-width: 1280px)',
  lgsm: '@media all and (min-width: 1024px) and (max-width: 1279px)',
  desktopMedia: '(min-width: 1024px)',
  tabletMedia: '(min-width: 768px) and (max-width: 1023px)',
  mobileMedia: '(max-width: 767px)',
};

export default breakpoints;
