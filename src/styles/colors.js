const colors = {
  primary: '#fff159',
  secondary: '#3483fa',
  danger: '#f75757',
  success: '#00a650',
  warning: '#febc11',
};

export default colors;
